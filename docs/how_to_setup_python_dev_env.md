# How to setup python dev environment

- Reference doc is [here](https://itnext.io/creating-a-modern-python-development-environment-3d383c944877)
## 1. install `python`

We will use [pyenv](https://github.com/pyenv/pyenv) to manange python version.
* Engineers can have multiple python binary versions installed in their machine and we need a way to switch between multiple versions easily.

### Prerequisite for Window | install `pyenv`
```sh
sudo apt update && sudo apt upgrade

sudo apt install -y build-essential git curl libexpat1-dev libssl-dev zlib1g-dev libncurses5-dev libbz2-dev liblzma-dev libsqlite3-dev libffi-dev tcl-dev linux-headers-generic libgdbm-dev libreadline-dev tk tk-dev

curl https://pyenv.run | bash
```

#### Posta install for pyenv | update your shell
append this at the end of your `~/.bashrc` or `~/.zshrc` file

```sh
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
```

### Prerequisite for MacOs | install `pyenv`

```sh
brew install pyenv
```

#### Posta install for pyenv | update your shell
append this at the end of your `~/.bashrc` or `~/.zshrc` file

```sh
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
```

### Install `python` via `pyenv`
At present `3.11` is the stabilized version since it won't add any new feature

```sh
# Install the required Python version:
pyenv install 3.11

# Refresh pyenv since you installed a new Python version:
pyenv rehash

# Set your global Python version:
pyenv global 3.11

# You can also set the local Python version, i.e. for a project:
# > pyenv local 3.11
# Confirm Python version:
pyenv version
3.11.9
```

### Install `poetry` to manage python library dependecies
> Poetry helps you declare, manage and install dependencies of Python projects, ensuring you have the right stack everywhere.

[poetry](https://github.com/python-poetry/poetry) make our life easier not to handle library dependencies ourselves

* [how to install poetry](https://python-poetry.org/docs/#installing-with-pipx)

```sh
pip install pipx
pipx install poetry
```

### (Recommended) Use pyenv-virtualenv to isolate projects libraries
[virtualenv](https://github.com/pyenv/pyenv-virtualenv) handles this situation smoothly

TL;DR I need to install different versions of numpy for these two projects for the same `python 3.11.9` binary
* project_a : numpy 1.x.x
* project_b : numpy 2.x.x

#### When this situation can be happening?
Ror the `project_a` I need to use a specific python library, let's say that it is `tecton(spark)`.
`tecton(spark)` has compatability issue with the latest `numpy, 2.x.x` so I need to use `numpy 1.x.x` for `project_a` rather than `numpy 2.x.x`
However for the `project_b` I am using spark so I like to use the latest numpy version

### (Optional) Setup Editorconfig to share code style configuration
> EditorConfig helps maintain consistent coding styles for multiple developers working on the same project across various editors and IDEs. The EditorConfig project consists of a file format for defining coding styles and a collection of text editor plugins that enable editors to read the file format and adhere to defined styles. EditorConfig files are easily readable and they work nicely with version control systems.

After setup your project add this snippet to `.editorconfig` file

```toml
# EditorConfig is awesome: http://EditorConfig.org

# top-most EditorConfig file
root = true

# Unix-style newlines with a newline ending every file
[*]
end_of_line = lf
insert_final_newline = true
trim_trailing_whitespace = true
charset = utf-8

# 4 space indentation
[*.py]
indent_style = space
indent_size = 4
``` 