# Set up python project with `poetry`

Let's set up our python project with `poetry` package.

The project name is `my_python_proj`

## 1. Create a new project with `poetry`

```sh
poetry new my_python_proj

# output is like below
Created package my_python_proj in my_python_proj
```

The structure of the project
1. the directory `my_python_proj` is to be your package name
2. the directory `tests` is to have your test files
3. pyproject.toml is `poetry` configuration file in toml file format

```sh
➜ tree .
.
├── README.md
├── my_python_proj
│   └── __init__.py
├── pyproject.toml
└── tests
    └── __init__.py                                                                                                                                                                                                                             2 directories, 4 files
```

## 2. Add some python packages for my project.
Let's say that my project is automated testing tool leveraging [secdev/scapy](https://github.com/secdev/scapy) package.

### (Essential for my project) Add scapy package
```sh
poetry add scapy

# output is like below
Using version ^2.5.0 for scapy

Updating dependencies
Resolving dependencies... (0.1s)                                                                                                                                                                                                                Package operations: 1 install, 0 updates, 0 removals                                                                                                                                                                                              - Installing scapy (2.5.0)
                                                                                                                        Writing lock file
```

You can see your `pyproject.toml` file has been updated

```toml
[tool.poetry.dependencies]
python = "^3.11"
scapy = "^2.5.0"
```

### (Optional but great to have) Add packages for better dev experience
In company projects are meant to be shared with other members for their contribution.
Therefore having these configuration will make your project easier to manage
0. Version control with git
1. Test and if your test can be covered by automated regression test mostly then test coverage
2. Code style
3. Linting
4. type check, `python` is dynamic typing language however type can be checked.
5. security check
6. git hook like I like to check all 5 things above before committing my change

#### 0. Version control with `git`
There are several options for source version control but [git](https://www.git-scm.com/) is the most popular one.
Therefore I will use it for my tutorial.

Let's initialise `git project`
```sh
git init
```

All other detailed tutorial for `git` will be skipped.

#### 1. Add package for testing

[pytest](https://docs.pytest.org/) is a default package for python test.

For your dev career, learning about [Test-driven development, TDD](https://en.wikipedia.org/wiki/Test-driven_development) will benefit you. You don't have to use it all the time but your mindset will be changed like that I should have automated regression test for my implementation.

```sh
poetry add --group dev pytest pytest-cov
```

Then your `pyproject.yaml` will be updated like below
* As you can see they were added under `tool.poetry.group.dev.dependencies` section. This means that this package will not be included your package but to be used while you are developing your package.

```toml
[tool.poetry.group.dev.dependencies]
pytest = "^8.2.2"
pytest-cov = "^5.0.0"
```

#### 2. Add code style with `editorconfig`
Engineers/People have their own preferred IDE/Editor for their development process.
And each IDE/Editor has their own code style setup differently.

However having the same code style can be benefit your mental model since your brain will not adopt a new code style. e.g. indentation, if-else, for and other syntax style.

We can configure this with [editorconfig](https://editorconfig.org/)

Add this snippet to `.editorconfig` file
* Feel free to add more or to modify the content as your style.
```toml
# EditorConfig is awesome: http://EditorConfig.org

# top-most EditorConfig file
root = true

# Unix-style newlines with a newline ending every file
[*]
end_of_line = lf
insert_final_newline = true
trim_trailing_whitespace = true
charset = utf-8

# 4 space indentation
[*.py]
indent_style = space
indent_size = 4
``` 

#### 3. Linting
Linting does
1. Static code analysis which can be helpful to prevent any potential mistake can cause production issues with small amount of cost.
2. Forcing code style

There are multiple options for this and [flake8](https://flake8.pycqa.org/en/latest/index.html#quickstart) is one of the popular ones

If you find anything suit you better, plz use it. This is just reference.

```sh
poetry add --group dev flake8
```

#### 4. Type check
Personally I like `static typed` language so I prefer to use [mypy](https://mypy-lang.org/).

>Mypy is an optional static type checker for Python that aims to combine the benefits of dynamic (or "duck") typing and static typing. Mypy combines the expressive power and convenience of Python with a powerful type system and compile-time type checking. Mypy type checks standard Python programs; run them using any Python VM with basically no runtime overhead.

Again, this is my personal preference if you fee, this is too verbose please skip this setup.

For me this is more readable
```python
def fib(n: int) -> Iterator[int]:
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a+b
```

Than this
```python
def fib(n):
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a+b
```

#### 5. Security check.
For python project, we do leverage lots of open-sourced packages.
And there are always possibilities that the packages you are using has security issues.
* Personally I have been using other services to check security issues, which companies pay for it. So this is my first time to use this package.

```sh
poetry add --group dev safety
```

#### 6. pre-commit check | Let's do all stuff above whenever you commit your code automatically.

We like to force all stuff above from 1 to 5 whenever engineers commit their code/changes in our repository.

[pre-commit](https://pre-commit.com/) will help you out with this

```sh
poetry add --group dev pre-commit

# Install pre-commit hook under .git
pre-commit install
## output is like below
pre-commit installed at .git/hooks/pre-commit

# Create a sample config file
pre-commit sample-config > .pre-commit-config.yaml
```

Now `pre-commit` will be executed whenever you commit your change
```
➜ g commit -m "init"
Trim Trailing Whitespace.................................................Passed
Fix End of Files.........................................................Passed
Check Yaml...............................................................Passed
Check for added large files..............................................Passed
[master (root-commit) ec42356] init
 6 files changed, 1240 insertions(+)
 create mode 100644 .pre-commit-config.yaml
 create mode 100644 README.md
 create mode 100644 my_python_proj/__init__.py
 create mode 100644 poetry.lock
 create mode 100644 pyproject.toml
 create mode 100644 tests/__init__.py
 ```

 ##### Add more hooks
 [Based on this doc](https://itnext.io/creating-a-modern-python-development-environment-3d383c944877), we can add our own hooks as well

 ```yaml
 - repo: local
    hooks:
      - id: flake8
        name: local flake8
        description: wemake-python-styleguide enforcement
        entry: poetry run flake8
        files: ^({PROJECT_NAME}/|tests/)
        args: ["--config=setup.cfg"]
        language: system
        types: [python]
      - id: mypy
        name: local mypy
        description: static type checker
        entry: poetry run mypy
        files: ^({PROJECT_NAME}/|tests/)
        language: system
        types: [python]
      - id: coverage
        name: local pytest coverage
        description: runs pytest along with coverage
        entry: poetry run pytest --cov {PROJECT_NAME} tests
        files: ^({PROJECT_NAME}/|tests/)
        language: system
        types: [python]
      - id: safety
        name: local safety
        description: check for vulnerabilities in packages.
        entry: poetry run safety check
        language: system
        types: [python]
        pass_filenames: false
 ```
