![image](https://camo.githubusercontent.com/37a3e8b8da37f0534f644164d0b2912b6c7e06ef9d87a4070c2135086f47eed2/68747470733a2f2f7365636465762e6769746875622e696f2f66696c65732f646f632f616e696d6174696f6e2d73636170792d696e7374616c6c2e737667)


# How `scapy` is being executed?
## 1. execute `sudo ./run_scapy`

아시다시피 unix 환경에서 이 명령어는 아래의 shell script 파일을 실행하는겁니다.
* https://github.com/secdev/scapy/blob/master/run_scapy

이 파일이 하는 일은 간단단하게
* 파이썬 바이너릴 셋팅후에 `python -m scapy`를 실행합니다.

## 2. `python -m scapy`는 어떻게 동작하나요?

`man python` 을 실행하시면 아래의 설명을 보여줍니다
```sh
 -m module-name
              Searches sys.path for the named module and runs the corresponding .py file as a script. This terminates
              the option list (following options are passed as arguments to the module).
```

### 2.a Python module에 대해서
파이썬에는 `module` 과 `package`라는 개념이 있습니다.
* module은 간단하게 하나의 python 파일이라고 생각하시면 됩니다. e.g. main.py
* package는 하나 이상의 python 파일을 가지고 있는 directory라고 생각하시면 됩니다. 다만 선행조건으로 `__init__.py` 파일이 directory 안에 있어야 합니다.

### 2.b package(directory)가 어떻게 python 실행파일이 되나요?

`package (directory)`가 모듈처럼 실행 가능합니다. 다만 선행 조건이 있습니다. 
아래 2개의 파일이 필요합니다. 상황에 따라서 다른 파일이 실행됩니다.
* `__init__.py`, https://github.com/secdev/scapy/blob/master/scapy/__init__.py
* `__main__.py`, https://github.com/secdev/scapy/blob/master/scapy/__main__.py

`__init__.py` 는 아래와 같이 python shell을 이용해서 `import scapy` 실행하실때 실행됩니다.
```sh
➜ python
Python 3.10.14 | packaged by conda-forge | (main, Mar 20 2024, 12:45:18) [GCC 12.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import sys
```

`__main__.py`는 아래와 같이 `-m` 옵션을 이행해서 모듈 혹은 패키지를 실행할때 사용됩니다. 
```
python -m scapy
```

:note: 관련해서 이 링크를 읽어보시면 좋아요. https://stackoverflow.com/questions/4042905/what-is-main-py

## 3. `__main__.py`는 뭘 하나요?
해당 파일 코드는 상당히 간단하네요
```python
from scapy.main import interact

interact()
```

1. scrapy directory 안에 `main.py` 파일안에 있는 interact라는 object를 import합니다, 이경우에는 함수네요
    * https://github.com/secdev/scapy/blob/master/scapy/main.py
2. 그리고 `interact()` 실행합니다.

## 4. `interact()` 는 무엇을 하나요?
코드 comment가 이야기 하듯이 프로그램의 entrypoint입니다. 
* https://github.com/secdev/scapy/blob/master/scapy/main.py#L702C1-L988
* 자세한 코드는 직접 읽어 보세요.

```python
def interact(mydict=None, argv=None, mybanner=None, loglevel=logging.INFO):
    # type: (Optional[Any], Optional[Any], Optional[Any], int) -> None
    """
    Starts Scapy's console.
    """
    # We're in interactive mode, let's throw the DeprecationWarnings
    warnings.simplefilter("always")

    # Set interactive mode, load the color scheme
    from scapy.config import conf
    conf.interactive = True
    conf.color_theme = DefaultTheme()
    if loglevel is not None:
        conf.logLevel = loglevel

    STARTUP_FILE = DEFAULT_STARTUP_FILE
    PRESTART_FILE = DEFAULT_PRESTART_FILE

    session_name = None

    if argv is None:
        argv = sys.argv
    ...
```
