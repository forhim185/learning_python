# learning_python
Let's set up our first python project.

## 1. Set up our python development environment .

Look at this [document](./docs/how_to_setup_python_dev_env.md)

## 2. Set up our python project.

Look at this [document](./docs/how_to_initialize_python_project.md)

## Appendix
1. [explain about `scapy` package](./docs/how_scapy_is_being_executed.md)